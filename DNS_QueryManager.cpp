#include<iostream>
#include<math.h>
#include<ldns/ldns.h>
#include<mysql++.h>


#include "DNS_QueryManager.h"
#include "DNS_Request.h"
#include "SQL_Manager.h"

using namespace std;

const vector<string> DNS_QueryManager::domains({"google.com",
		"youtube.com",
		"facebook.com",
		"baidu.com",
		"wikipedia.com",
		"yahoo.com",
		"google.co.in",
		"reddit.com",
		"qq.com",
		"taobao.com"});	

DNS_QueryManager::DNS_QueryManager()
{
	resolver_ = NULL;
}

DNS_QueryManager::~DNS_QueryManager()
{
	if(resolver_)
	{
		ldns_resolver_free(resolver_);
		resolver_ = NULL; 
	}
}

int DNS_QueryManager::CreateResolver()
{
	ldns_status status = ldns_resolver_new_frm_file(&resolver_, NULL);
	if(status != LDNS_STATUS_OK)
	{
		cout << "Error creating resolver" << endl;
		return 0;
	}
	return 1;
}

int DNS_QueryManager::QueryForDomain(const string & domain_name, 
		uint32_t & latency, 
		string & query_timestamp)
{
	DNS_Request *dns_request = new DNS_Request(domain_name);
	int status = dns_request->SendDNSQuery(resolver_);
	if(!status)
	{
		cout << "Error while querying for domain " << domain_name << endl;
		return 0;
	}
	latency = dns_request->GetLatency();
	query_timestamp = dns_request->GetQueryTimestamp();

	delete dns_request;
	return 1;

}

int DNS_QueryManager::DoDNSQueries()
{
	/*using the same resolver for all queries*/
	int status = CreateResolver();
	if(!status)
	{	
		cout << "Resolver error" << endl;
		return 0;
	}


	for(int i =0; i < domains.size(); i++)
	{
		uint32_t latency;
		string query_timestamp;
		QueryForDomain(domains[i], latency, query_timestamp);
		if(!SaveDNSQuery(domains[i], latency, query_timestamp))
		{
			cout << "Error while saving query to database" << endl;
			return 0;
		}
		if(!UpdateStatistics(domains[i], latency, query_timestamp))
		{
			cout << "Error while updating statistics" << endl;
			return 0;
		}
	}
	return 1;

}

int DNS_QueryManager::SaveDNSQuery(const string & domain, 
		const uint32_t latency, 
		const string & query_timestamp)
{
	mysqlpp::Connection conn;
	SQL_Manager sql_mgr(conn);
	if(!sql_mgr.CreateConnection())
	{
		cout << "Error connecting to database" << endl;
		return 0;
	}
	string sql = "INSERT INTO samples (site_name, latency, query_start_time) ";
	sql +="VALUES ( '" + domain + "', "+ to_string(latency) + ", '"+ query_timestamp +"')";

	if(!sql_mgr.DBWrite(sql))
	{
		cout << "Error executing insert" << endl;
		return 0;
	}	


	return 1;

}


int DNS_QueryManager::UpdateStatistics(const string & domain, 
		const uint32_t latency, 
		const string & query_timestamp)
{
	mysqlpp::Connection conn;
	SQL_Manager sql_mgr(conn);
	if(!sql_mgr.CreateConnection())
	{
		cout << "Error connecting to database" << endl;
		return 0;
	}
	mysqlpp::Row row;
	if(!sql_mgr.ReadStatisticsRow(domain, row))
	{
		cout << "Error reading old statistics" << endl;
		return 0;
	}

	double average = 0.0;
	double std_dev = 0.0;
	uint32_t no_of_records = 0;

	string sql = ""; 
	if(!row.empty())
	{
		if(!Average(row, average, latency))
		{
			cout << "Error computing average" << endl;
			return 0;
		}
		if(!StandardDeviation(row, std_dev, latency))
		{
			cout << "Error computing standard deviation" << endl;
			return 0;
		}
		no_of_records = row[4];

		sql = "UPDATE statistics SET average=";
		sql += to_string(average)+ ", standard_deviation=" + to_string(std_dev)
			+ ", no_of_queries=" + to_string(no_of_records+1) + ", last_query_timestamp='"
			+ query_timestamp + "' WHERE site_name='"+ domain + "'";
	}
	else
	{
		average = latency;
		std_dev = latency;
		no_of_records += 1;

		sql = "INSERT INTO statistics (site_name, average, standard_deviation, no_of_queries,"
			"first_query_timestamp, last_query_timestamp) VALUES("; 
		sql += "'" + domain + "', " +to_string(average)+", " + to_string(std_dev) 
			+ ", " + to_string(no_of_records+1) + ", '" 
			+ query_timestamp + "', '" + query_timestamp + "')"; 
	}


	if(!sql_mgr.DBWrite(sql))
	{
		cout << "Error executing insert" << endl;
		return 0;
	}

	return 1;
}


//Compute online average - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
int DNS_QueryManager::Average(const mysqlpp::Row & row, double & average, const uint32_t latency)
{
	if(row.size() > 6)
	{
		double old_average = row[2];
		uint32_t no_of_records = row[4];
		average = (old_average * no_of_records + latency) / (no_of_records+1);

		return 1;
	}

	cout << "Invalid information" << endl;
	return 0;
}


//Compute online variance - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
int DNS_QueryManager::StandardDeviation(const mysqlpp::Row & row, double & std_dev, const uint32_t latency)
{

	if(row.size() > 6)
	{
		double new_average;
		Average(row, new_average, latency);
		double old_average = row[2];
		uint32_t old_no_of_records = row[4];
		double old_std_dev = row[3];

		std_dev = (old_no_of_records * pow(old_std_dev,2) +
				(latency - old_average)*(latency - new_average))/(old_no_of_records+1);
		std_dev = sqrt(std_dev);


		return 1;
	}

	cout << "Invalid information" << endl;
	return 0;

}



















