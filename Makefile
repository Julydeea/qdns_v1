
CXX=g++
CFLAGS=-Wall -Wfatal-errors

INCLUDE= -I./mysql++/lib \
		 -I/usr/include/mysql \
		 -I./ldns-1.7.0

LIBS= -L./ldns-1.7.0/lib \
           -L./mysql++ \
           -lmysqlpp \
		   -lldns 

SOURCES =  main.cpp \
		   SQL_Manager.cpp \
		   DNS_Request.cpp \
	       DNS_QueryManager.cpp

EXECUTABLE=main


all: $(SOURCES) $(EXECUTABLE)


$(EXECUTABLE): $(SOURCES)
	$(CXX) $(CFLAGS) $(INCLUDE) $(SOURCES) -o $@ $(LIBS)

clean:
	rm main
