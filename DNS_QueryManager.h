#ifndef LDNS_QUERYMANAGER_H
#define LDNS_QUERYMANAGER_H

#include<vector>
#include<string>
#include<mysql++.h>

class DNS_QueryManager 
{
	public:
		DNS_QueryManager();
		~DNS_QueryManager();
		DNS_QueryManager(const DNS_QueryManager &) = delete; 
		DNS_QueryManager operator=(const DNS_QueryManager&) = delete;

		int DoDNSQueries();
		ldns_resolver* GetResolver() { return resolver_; }

		static const std::vector<std::string> domains;
	private:
		int CreateResolver();
		int QueryForDomain(const std::string & , uint32_t & , std::string & );
		int SaveDNSQuery(const std::string &, const uint32_t, const std::string &);
		int UpdateStatistics(const std::string & , const uint32_t, const std::string & );
		int Average(const mysqlpp::Row &, double &, const uint32_t);
		int StandardDeviation(const mysqlpp::Row &, double &, const uint32_t); 
		ldns_resolver *resolver_;
};





#endif


