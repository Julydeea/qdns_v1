#include <iostream>
#include <chrono>
#include <thread>
#include <typeinfo>
#include <ldns/ldns.h>

#include "DNS_QueryManager.h"

using namespace std;
using namespace std::chrono;

int main(int argc, char *argv[])
{
	uint32_t query_period;
	if(argc != 2)
	{
		cout << "Please provide query frequency: " << endl;
		cin >> query_period;
	}
	else
	{
		query_period = atoi(argv[1]);
	}

	DNS_QueryManager qm;
	while(true)
	{

		high_resolution_clock::time_point start = high_resolution_clock::now();
		if(!qm.DoDNSQueries())
		{
			return 0;
		}
		high_resolution_clock::time_point stop = high_resolution_clock::now();

		uint32_t elapsed = duration_cast<milliseconds> (stop-start).count(); 	

		if (query_period > elapsed)
		{
			this_thread::sleep_for(duration<int, ratio<1,1000> >(query_period - elapsed));
		}
	} 

	return 1;
}
